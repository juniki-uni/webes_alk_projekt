package webapp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import java.sql.Date;
import webapp.entities.Album;
import webapp.entities.Customer;
import webapp.repositories.AlbumRepository;
import webapp.repositories.ArtistRepository;
import webapp.repositories.CustomerRepository;
import webapp.services.AlbumService;
import webapp.services.ArtistService;
import webapp.services.CustomerService;
import webapp.services.OrderService;

@SpringBootApplication 
public class Application {
	
	/*If mariaDB is used
	 * In order to make this app work the following steps are required beforehand:
	 * (1) Start Mariadb server as follows:
	 * 		(First time create system tables: run bin/mysql_install_db.exe)
			To start MariaDB: run bin/mysqld.exe --console
	 * (2) Start HeidiSQL
	 * (3) Login as admin - user:root
	 * (4) Create a database with name: userdb2
	 * (5) In Tools/UserManager allow full access for user:mariadb (pw:mariadb123) to the userdb2 database.
	 * 
	 * Start http://localhost:8180/
	 */
	
	/*IF H2 database is used:
	 * Start http://localhost:8180/
	 * You can check the database
	 * http://localhost:8180/h2-console/
	 * Driver class: org.h2.Driver
	 * JDBC URL: jdbc:h2:mem:testdb
	 * user: sa
	 * pw:
	 * 
	 */
	
	@Autowired
	AlbumService albumService;
	@Autowired
	ArtistService artistService;
	@Autowired
	AlbumRepository albumRepository;
	@Autowired
	CustomerRepository customerRepository;
	@Autowired
	CustomerService customerService;
	@Autowired
	OrderService orderService;
	
	@Bean
	public CommandLineRunner demo() {
		return (args) -> {
			//adding a few albums
			
			long album1 = albumService.addAlbum("The Wall",(float)10.5,(long)3,"Rock","Ez egesz jo zeneleiras",null);
			long album2 = albumService.addAlbum("Circus",(float)5,(long)11,"POP","Its britney bitch","circus.jpg");
			long album3 = albumService.addAlbum("Stereohype",(float)5,(long)11,"Dance","Its fridaaaay",null);
			
			//adding few artists
			long artist1 = artistService.addArtist("Pink Floyd");
			long artist2 = artistService.addArtist("Britney Spears");
			long artist3 = artistService.addArtist("James Hype");
			
			albumService.addAlbumArtist(album1, artist1);
			albumService.addAlbumArtist(album2, artist2);
			albumService.addAlbumArtist(album3, artist3);
			

			
			
			
			System.out.println("Demo database content added");
			System.out.println("Number of albums: " + albumRepository.count());

			System.out.println("Album with the requested id "+ album2 + " is: " + albumRepository.findByAlbumId(album2).get(0).getTitle());

			
			customerService.addCustomer("admin@vinylstore.com", "pass123", "Admin Janos", "+36123456789", "Budapest, BME");
			System.out.println(customerRepository.findByEmail("admin@vinylstore.com").getPassword());
			Customer custWhoOrders = customerRepository.findByEmail("admin@vinylstore.com");
			long custWhoOrdersId=custWhoOrders.getId();
			System.out.println("Customer's id is: "+ custWhoOrdersId);
			
			long order1 = orderService.addOrder(custWhoOrdersId);  //returns the ID of the added order
			orderService.addOrderItem(order1, album2, 1);
			
			
			
			
//			albumRepository.findByAlbumId(album1).get(0).getArtists().size();
//			long b = albumRepository.findByAlbumId(album1).get(0).getArtists().iterator().next().getArtistId();
//			boolean b = albumRepository.findByAlbumId(album1).get(0).getArtists().isEmpty();
//			System.out.println("has next --- " + b);
//			System.out.println("The artist of " + albumRepository.findByAlbumId(album2).get(0).getTitle() + " is " + albumService.getAlbumArtist(album2).getArtistName());
//			albumService.getAlbumsByTitle("Circus");
//			albumService.getAllAlbums();
			

			
		};
	}
	
	
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
    
}
