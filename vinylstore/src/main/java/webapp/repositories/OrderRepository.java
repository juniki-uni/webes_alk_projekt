package webapp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import webapp.entities.Customer;
import webapp.entities.Order;

@Repository
public interface OrderRepository extends CrudRepository<Order, Long> {
	List<Order> findAllByCustomer(Customer customer);
	
//	@Query("SELECT SUM(a.price) from Album a, OrderLine ol, Order o where p.prodId=ol.product AND o.orderId=ol.order AND o.orderId=?1")
//	float getInvoiceTotal(long orderId);
}
