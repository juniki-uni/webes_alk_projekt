package webapp.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;
import webapp.entities.Customer;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, Long>{
	Customer findByEmail(String email);
}
