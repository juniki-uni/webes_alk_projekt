package webapp.repositories;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import webapp.entities.Album;
import webapp.entities.Artist;

@Repository
public interface ArtistRepository extends CrudRepository<Artist, Long> {
}
