package webapp.repositories;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import webapp.entities.OrderLine;
import webapp.entities.OrderLineId;

@Repository
public interface OrderLineRepository extends CrudRepository <OrderLine, OrderLineId> {
	List<OrderLine> findByIdOrderId(long orderId);
}
