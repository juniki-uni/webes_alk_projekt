package webapp.repositories;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import webapp.entities.Album;


@Repository
public interface AlbumRepository extends CrudRepository<Album, Long> {
	List<Album> findByTitle(String title);
	List<Album> findAll();
	List<Album> findByAlbumId(long albumId);
}
