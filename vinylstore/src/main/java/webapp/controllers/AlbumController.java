package webapp.controllers;

import javax.validation.Valid;

import org.junit.validator.PublicClassValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import webapp.entities.Album;
import webapp.repositories.AlbumRepository;
import webapp.services.AlbumService;

@Controller
public class AlbumController {
    
    private final AlbumRepository albumRepository;

    @Autowired
    public AlbumController(AlbumRepository albumRepository) {
        this.albumRepository = albumRepository;
    }
    
    
   
   @GetMapping("/")
   public String browseCatalog(Model model) {
	   
	   model.addAttribute("albums", albumRepository.findAll());
	   return "browse";
   }
   
   @GetMapping("/product/{id}")
   public String viewProduct(@PathVariable("id") long id, Model model) {
	   albumRepository.findByAlbumId(id).get(0).getTitle();
	   model.addAttribute("album", albumRepository.findByAlbumId(id).get(0));
	   return "product";
   }
    
   @GetMapping("/login")
   public String showLogin() {
   	return "login";
   }
    
}
