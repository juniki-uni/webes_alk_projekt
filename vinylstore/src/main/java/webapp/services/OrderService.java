package webapp.services;

import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import webapp.entities.Customer;
import webapp.entities.Order;
import webapp.entities.OrderLine;
import webapp.entities.Album;
import webapp.repositories.CustomerRepository;
import webapp.repositories.OrderLineRepository;
import webapp.repositories.OrderRepository;
import webapp.repositories.AlbumRepository;

@Service
public class OrderService {

	@Autowired
	private OrderRepository orderRepo;
	@Autowired
	private CustomerRepository customerRepo;
	@Autowired
	private AlbumRepository albumRepo;
	@Autowired
	private OrderLineRepository orderLineRepo;
	
	public long addOrder(long custId){
		long id = 0;
		try {
			Customer cust = customerRepo.findById(custId).get();
			Order o = new Order();
			o.setCustomer(cust);
			orderRepo.save(o);
			id = o.getOrderId();
		}catch (NoSuchElementException e) {
			e.printStackTrace();
		}
		return id;
	}
	
	public void addOrderItem(long orderId, long productId, int quantity){
		try {
			Order order = orderRepo.findById(orderId).get();
			Album prod = albumRepo.findById(productId).get();
			OrderLine orderLine = new OrderLine();
			orderLine.setOrder(order);
			orderLine.setAlbum(prod);
			orderLine.setQuantity(quantity);
			orderLineRepo.save(orderLine);
		}catch (NoSuchElementException e) {
			e.printStackTrace();
		}
	}
	
//	public void setInvoiceDetails(long orderId, Date paidOn){
//		try {
//			Order order = orderRepo.findById(orderId).get();
//			Invoice invoice = order.getInvoice();
//			invoice.setTotal(orderRepo.getInvoiceTotal(orderId));
//			invoice.setPaidOn(paidOn);
//			
//			//Updating the orders. The relating invoices will be saved automatically. 
//			//See "cascade = CascadeType.ALL" in Order class.
//			orderRepo.save(order); 	
//		}catch (NoSuchElementException e) {
//			e.printStackTrace();
//		}
//	}
	
	public void deleteOrder(long orderId) {
		try {
			Order order = orderRepo.findById(orderId).get();
			List<OrderLine> olsToDel = orderLineRepo.findByIdOrderId(orderId);
			
			//To delete an order, first the relating order lines should be deleted.
			//The invoice relating to the order is deleted automatically.
			//See "cascade = CascadeType.ALL" in Order class.
			for(OrderLine ol : olsToDel) {
				orderLineRepo.delete(ol);
			}
			orderRepo.delete(order);

		}catch (NoSuchElementException e) {
			e.printStackTrace();
		}		
	}
}