package webapp.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import webapp.entities.Customer;
import webapp.repositories.CustomerRepository;

@Service
public class CustomerService {
	@Autowired
	CustomerRepository customerRepository;
	
	public void addCustomer(String email, String password, String name, String phone, String address){
		Customer c = new Customer();
		c.setEmail(email);
		c.setPassword(password);
		c.setName(name);
		c.setPhone(phone);
		c.setAddress(address);
		customerRepository.save(c);
	}
	
	
	
}
