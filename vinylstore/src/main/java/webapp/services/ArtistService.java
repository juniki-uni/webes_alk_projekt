package webapp.services;

import java.util.List;
import java.util.NoSuchElementException;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import webapp.entities.Album;
import webapp.entities.Artist;
import webapp.repositories.AlbumRepository;
import webapp.repositories.ArtistRepository;

@Service
public class ArtistService {
	@Autowired
	private ArtistRepository artistRepository;
	
	public long addArtist(String artistName) {
		Artist ar =new Artist();
		ar.setArtistName(artistName);
		artistRepository.save(ar);
		return ar.getArtistId();
	}
}
