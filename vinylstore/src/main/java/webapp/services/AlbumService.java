package webapp.services;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import javax.transaction.Transactional;

import java.util.HashSet;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import webapp.entities.Album;
import webapp.entities.Artist;
import webapp.repositories.AlbumRepository;
import webapp.repositories.ArtistRepository;

@Service
public class AlbumService {
	
	@Autowired
	private AlbumRepository albumRepository;
	@Autowired
	private ArtistRepository artistRepository;
	
	public long addAlbum(String title, float price, long quantity, String style, String description, String albumPic) {
		Album a = new Album();
		a.setTitle(title);
		a.setPrice(price);
		a.setQuantity(quantity);
		a.setStyle(style);
		a.setDescription(description);
		a.setAlbumPic(albumPic);
		albumRepository.save(a);
		return a.getAlbumId();
	}
	
//	public Artist getAlbumArtist(long albumId) {
//	Album a = albumRepository.findByAlbumId(albumId).get(0);
//	Set<Artist> art = a.getArtists();
//	return art.iterator().next();
//}
	
	@Transactional
	public void addAlbumArtist(long albumId, long artistId) {
		try {
			Album album = albumRepository.findById(albumId).get();
			Artist artist = artistRepository.findById(artistId).get();
			album.getArtists().add(artist);
			albumRepository.save(album);
		}catch (NoSuchElementException e) {
			e.printStackTrace();
		}
	}
	
//	public void getAlbumsByTitle(String title) {
//		List<Album> albumList = albumRepository.findByTitle(title);
//		System.out.println("\nResult of search by title: " + title);	
//		for(int i=0; i<albumList.size(); i++){
//			System.out.println(i+1 + "  "+albumList.get(i).getTitle() + " " + albumList.get(i).getAlbumPic());
//		}
//		System.out.println();
//	}
//	
//	public void getAllAlbums() {
//		List<Album> albumList = albumRepository.findAll();
//		System.out.println("\nAll albums listed here: ");	
//		for(int i=0; i<albumList.size(); i++){
//			System.out.println(i+1 + "  "+albumList.get(i).getTitle() + " " + albumList.get(i).getAlbumPic());
//		}
//		System.out.println();
//	}
//	
//	public void getAlbumsById(long albumId) {
//		List<Album> albumList = albumRepository.findByAlbumId(albumId);
//		System.out.println("\nResult of search by id: " + albumId);	
//		for(int i=0; i<albumList.size(); i++){
//			System.out.println(i+1 + "  "+albumList.get(i).getTitle() + " " + albumList.get(i).getAlbumPic());
//		}
//		System.out.println();
//	}
	

	

}
