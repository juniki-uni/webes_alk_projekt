package webapp.entities;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;

@Entity
public class OrderLine{
	
    @EmbeddedId
    private OrderLineId id = new OrderLineId();
	
    @ManyToOne (optional = false)
    @MapsId("orderId")
    private Order order;

    @ManyToOne (optional = false)
    @MapsId("albumId")
    private Album album;

	@Column (nullable = false)
    private int quantity;
    
    public OrderLine() {
    }

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public Album getAlbum() {
		return album;
	}

	public void setAlbum(Album album) {
		this.album = album;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}    
}
