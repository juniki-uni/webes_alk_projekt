package webapp.entities;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;

import webapp.entities.OrderLine;
import webapp.entities.Artist;


@Entity
public class Album {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "albumId")
	private long albumId;

	@Column(nullable = false)
	private String title;

	@Column(nullable = false)
	private float price;
	
	@Column
	private long quantity;
	
	@Column
	private String style;
	
	@Column
	private String description;
	
	@Column
	private String albumPic;
	
	@OneToMany(mappedBy = "album")
	private Set<OrderLine> orderLines;

	@ManyToMany
	@JoinTable(name="album_artist",
			   joinColumns = @JoinColumn(name = "album_id"),
			   inverseJoinColumns = @JoinColumn(name = "artist_id"))
    private Set<Artist> artists = new HashSet<>();

	public Album() {
	}
	
	public Album(String title, float price, long quantity, String style,
			String description, String albumPic) {
		super();
		this.title = title;
		this.price = price;
		this.quantity = quantity;
		this.style = style;
		this.description = description;
		this.albumPic = albumPic;
	}

	public long getAlbumId() {
		return albumId;
	}

	public void setAlbumId(long albumId) {
		this.albumId = albumId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public long getQuantity() {
		return quantity;
	}

	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Set<Artist> getArtists() {
		return artists;
	}

	public void setArtists(Set<Artist> artists) {
		this.artists = artists;
	}

	public String getAlbumPic() {
		return albumPic;
	}

	public void setAlbumPic(String albumPic) {
		this.albumPic = albumPic;
	}
	
	
	@Override
	public String toString() {
		return "Album [albumId=" + albumId + ", title=" + title + ", price=" + price + ", quantity=" + quantity
				+ ", style=" + style + ", description=" + description + ", artists="
				+ artists + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (albumId ^ (albumId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Album other = (Album) obj;
		if (albumId != other.albumId)
			return false;
		return true;
	}
	
	
	
}
