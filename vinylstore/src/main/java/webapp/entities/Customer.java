package webapp.entities;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import webapp.entities.Order;

@Entity
@Table (name = "Customer")
public class Customer {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column (name = "cid")
	private long customerId;
	@Column
	private String email;
	@Column
	private String password;
	@Column
	private String name;
	@Column
	private String phone;
	@Column
	private String address;
	
    @Transient
    boolean logged_in;
    
    @OneToMany(mappedBy="customer",	fetch = FetchType.EAGER)
    private Set<Order> orders;
	
	public Customer() {
	}
	
	public Customer(String email, String password, String name, String phone, String address) {
		super();
		this.email = email;
		this.password = password;
		this.name = name;
		this.phone = phone;
		this.address = address;
	}
	
	public long getId() {
		return customerId;
	}

	public void setId(long customerId) {
		this.customerId = customerId;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
}
