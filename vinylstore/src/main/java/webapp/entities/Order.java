package webapp.entities;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table (name = "P_order")
public class Order {
	
    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    @Column (name = "oid")
    private long orderId;
    
    @Column (nullable = false)
    private Date orderDate;

    @ManyToOne (optional = false)
    @JoinColumn(name = "cid")
    private Customer customer;

    @OneToMany (mappedBy = "order")
    private Set<OrderLine> orderLines;
    
    public Order() {
    	this.orderDate = new Date();    	
    }
        
    public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public long getOrderId() {
		return orderId;
	}
}
