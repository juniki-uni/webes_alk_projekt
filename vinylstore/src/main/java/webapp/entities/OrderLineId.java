package webapp.entities;

import java.io.Serializable;
import javax.persistence.Embeddable;

@Embeddable
public class OrderLineId implements Serializable{
	
	private long orderId;
	
	private long albumId;
}